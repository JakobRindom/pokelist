//
//  AppDelegate.swift
//  PokeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if let pokelist = UIStoryboard(name: "PokeList", bundle: nil).instantiateInitialViewController() {
           let navigation = UINavigationController(rootViewController: pokelist)

            let frame = UIScreen.main.bounds
            window = UIWindow(frame: frame)

            window!.rootViewController = navigation
            window!.makeKeyAndVisible()
        }
        
        return true
    }

}

