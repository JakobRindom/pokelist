//
//  DataLayer.swift
//  PókeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import Foundation

class Datalayer: NSObject {

    /**
     Share datalayer instance
     
     - returns: Datalayer
     */
    static let shared: Datalayer = Datalayer()

    // Url for the backend
    static var backendUrl: String {
        return "https://pokeapi.co/api/v2/"
    }

    private var requestHandler: RequestHandler

    override init() {
        self.requestHandler = RequestHandler()
        super.init()
    }

    func get<T>(handler: RequestHandlerProtocol, completion: @escaping (RequestResult<T>) -> Void) {
        self.requestHandler.performRequest(handler: handler) { result in
            switch (result) {
            case let .success(obj as T):
                completion(RequestResult.success(obj))
            case .error(let message):
                completion(RequestResult.error(message))
            case .offline:
                completion(RequestResult.offline)
            default:
                completion(RequestResult.error(.noObjectsReturned))
            }
        }
    }
    
    /// Get list of pokemons
    func getPokoList(offset: Int ,completion: @escaping (RequestResult<PokemonListModel>) -> Void) {
        let requestHandler = GetPokemonListRequest(offset: offset)
        get(handler: requestHandler, completion: completion)
        
    }
   
    /// Get pokemon Details
    func getPokemonDetails(chosenPokemonId: String ,completion: @escaping (RequestResult<PokemonDetailsModel>) -> Void) {
        let requestHandler = GetPokemonDetailsRequest(chosenPokemonId: chosenPokemonId)
        get(handler: requestHandler, completion: completion)
        
    }
    
    /// Get pokemon pokemon-species
    func getPokemonDetailSpecies(chosenPokemonId: String ,completion: @escaping (RequestResult<PokemonDetailSpiciesModel>) -> Void) {
        let requestHandler = GetPokemonDetailSpiciesRequest(chosenPokemonId: chosenPokemonId)
        get(handler: requestHandler, completion: completion)
    }
    
    /// Get pokemon pokemon-species
    func getPokemonEvolutionChain(chosenPokemonId: String ,completion: @escaping (RequestResult<PokemonEvolutionModel>) -> Void) {
        let requestHandler = GetPokemonDetailEvolutionRequest(chosenPokemonId: chosenPokemonId)
        get(handler: requestHandler, completion: completion)
    }
    
    
    
    
    
    
}
