//
//  PokemonDetailSpiciesModel.swift
//  PokeList
//
//  Created by Jakob Rindom on 13/01/2021.
//

import Foundation

// MARK: - PokemonDetailSpiciesModel
struct PokemonDetailSpiciesModel: Codable {
    let captureRate: Int?
    let evolutionChain: EvolutionChain?
    let genera: [Genus]?
    let growthRate: GrowthRate?
    let name: String?
    let names: [Name]?
    
    enum CodingKeys: String, CodingKey {
        case captureRate = "capture_rate"
        case evolutionChain = "evolution_chain"
        case genera
        case growthRate = "growth_rate"
        case name, names
    }
}

// MARK: - EvolutionChain
struct EvolutionChain: Codable {
    let url: String?
}

// MARK: - Genus
struct Genus: Codable {
    let genus: String?
    let language: GrowthRate?
}

// MARK: - GrowthRate
struct GrowthRate: Codable {
    let name: String?
    let url: String?
}

// MARK: - Name
struct Name: Codable {
    let language: GrowthRate?
    let name: String?
}

