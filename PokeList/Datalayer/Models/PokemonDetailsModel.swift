//
//  PokeDetailsModel.swift
//  PokeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import UIKit
import Foundation

// MARK: - PokemonDetailsModel
struct PokemonDetailsModel: Codable {
    let abilities: [Ability]?
    let baseExperience, height, id: Int?
    let name: String?
    let species: Species?
    let sprites: Sprites?
    let stats: [Stat]?
    let types: [TypeElement]?
    let weight: Int?

    enum CodingKeys: String, CodingKey {
        case abilities
        case baseExperience = "base_experience"
        case height, id, name, species, sprites, stats, types, weight
    }
}

// MARK: - Ability
struct Ability: Codable {
    let ability: Species?
    let isHidden: Bool?
    let slot: Int?

    enum CodingKeys: String, CodingKey {
        case ability
        case isHidden = "is_hidden"
        case slot
    }
}

// MARK: - Species
struct Species: Codable {
    let name: String?
    let url: String?
}

// MARK: - Sprites
struct Sprites: Codable {
    let other: Other?
}

// MARK: - Other
struct Other: Codable {
    let officialArtwork: OfficialArtwork?

    enum CodingKeys: String, CodingKey {
        case officialArtwork = "official-artwork"
    }
}

// MARK: - OfficialArtwork
struct OfficialArtwork: Codable {
    let frontDefault: String?

    enum CodingKeys: String, CodingKey {
        case frontDefault = "front_default"
    }
}

// MARK: - Stat
struct Stat: Codable {
    let baseStat, effort: Int?
    let stat: Species?

    enum CodingKeys: String, CodingKey {
        case baseStat = "base_stat"
        case effort, stat
    }
}


// MARK: - TypeElement
struct TypeElement: Codable {
    let slot: Int?
    let type: TypeSpec?
}

struct TypeSpec: Codable {
    let url: String?
    let name: PokemonSpiciestype?
    //let type: PokemonSpiciestype?
}


public enum PokemonSpiciestype: String, Codable {
    case normal
    case fire
    case water
    case electric
    case grass
    case ice
    case fighting
    case poison
    case ground
    case flying
    case psychic
    case bug
    case rock
    case ghost
    case dragon
    case dark
    case steel
    case fairy
    case unknown
    
    func color() -> UIColor {
        switch self {
        case .normal:
            return UIColor(hex: "797964")
        case .fire:
            return UIColor(hex: "ff3a2a")
        case .water:
            return UIColor(hex: "0080ff")
        case .electric:
            return UIColor(hex: "ffcc33")
        case .grass:
            return UIColor(hex: "5cb737")
        case .ice:
            return UIColor(hex: "66ccff")
        case .fighting:
            return UIColor(hex: "a84d3d")
        case .poison:
            return UIColor(hex: "88447a")
        case .ground:
            return UIColor(hex: "bf9926")
        case .flying:
            return UIColor(hex: "556dff")
        case .psychic:
            return UIColor(hex: "ff227a")
        case .bug:
            return UIColor(hex: "83901a")
        case .rock:
            return UIColor(hex: "a59249")
        case .ghost:
            return UIColor(hex: "5454b3")
        case .dragon:
            return UIColor(hex: "4e38e9")
        case .dark:
            return UIColor(hex: "573e31")
        case .steel:
            return UIColor(hex: "8e8ea4")
        case .fairy:
            return UIColor(hex: "e76de7")
        case .unknown:
            return UIColor(hex: "797964")
        }
    }
}

extension PokemonSpiciestype {
    public init(from decoder: Decoder) throws {
        guard let rawValue = try? decoder.singleValueContainer().decode(String.self) else {
            self = .unknown
            return
        }
        self = PokemonSpiciestype(rawValue: rawValue) ?? .unknown
    }
}

