//
//  PokemonEvolutionModel.swift
//  PokeList
//
//  Created by Jakob Rindom on 13/01/2021.
//


import Foundation

// MARK: - PokemonEvolutionModel
struct PokemonEvolutionModel: Codable {
    let chain: Chain?
    let id: Int?
}

// MARK: - Chain
struct Chain: Codable {
    let evolutionDetails: [EvolutionDetail]?
    let evolvesTo: [Chain]?
    let species: Species?

    enum CodingKeys: String, CodingKey {
        case evolutionDetails = "evolution_details"
        case evolvesTo = "evolves_to"
        case species
    }
}

// MARK: - EvolutionDetail
struct EvolutionDetail: Codable {
    let minLevel: Int?

    enum CodingKeys: String, CodingKey {
        case minLevel = "min_level"
    }
}
