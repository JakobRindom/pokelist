//
//  PokelistModel.swift
//  PókeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import Foundation

struct PokemonListModel: Codable {
    
    struct Pokemons: Codable {
        let name: String?
        let url: String?
    }
    
    let next: String?
    let previous: String?
    let count: Int
    
    var results: [Pokemons]
    
    
}
