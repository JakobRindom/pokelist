//
//  NetworkError.swift
//  PókeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import Foundation

enum NetworkError: Error {
    case offline//"Ingen forbindelse til internettet."
    case badUrl
    case noObjectsReturned//"No objects returned"
  //  case notImplemented//"Not implemented"
    case jsonFormattedIncorrectly//"Error: JSON formatted incorrectly" or "JSON not formatted correctly" or "Error: JSON formatted incorrectly in \(json)"
    case serverError(String)
    case serverErrorCode(String)
    case noInformation//"Kunne ikke finde information." or "JSON object was empty:\n\(String(describing: params()))" or "JSON object was empty"
 
    case jsonDecodingError(Error)//"\(error.localizedDescription)"
}

