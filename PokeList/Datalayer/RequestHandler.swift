//
//  RequestHandler.swift
//  PókeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import Foundation

enum RequestResult<T> {
    case success(T)
    case error(NetworkError)
    case offline
}

class RequestHandler {
    
    func performRequest(handler: RequestHandlerProtocol, completion: @escaping ((RequestResult<Any>) -> (Void))) {
        
        let session = URLSession.shared
        let urlString = handler.urlString(baseUrl: Datalayer.backendUrl)
        guard var urlComponents = URLComponents(string: urlString) else {
            return completion(RequestResult.error(.badUrl))
        }
        if let params = handler.params() {
            let queryItems = params.map {
                return URLQueryItem(name: "\($0)", value: "\($1)")
            }
            urlComponents.queryItems = queryItems
        }
        guard let url = urlComponents.url else {
            return completion(RequestResult.error(.badUrl))
        }
        
        let task = session.dataTask(with: url) { data, response, error in
            
            if error != nil || data == nil {
                return completion(RequestResult.error(.noObjectsReturned))
            }
            
            guard let urlResponse = response as? HTTPURLResponse, (200...299).contains(urlResponse.statusCode) else {
                return completion(RequestResult.error(.serverErrorCode("ErrorCode: \((response as? HTTPURLResponse)?.statusCode ?? 500) ") ))
            }
            
            guard let mime = urlResponse.mimeType, mime == "application/json" else {
                return completion(RequestResult.error(.serverError("Wrong MIME type!") ))
            }
            
            DispatchQueue.global(qos: .userInitiated) .async {
                let result = handler.mapResult(data: data!)
                DispatchQueue.main.async {
                    completion(result)
                }
            }
        }
        task.resume()
    }
    
}
