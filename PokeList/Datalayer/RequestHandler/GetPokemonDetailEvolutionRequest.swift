//
//  GetPokemonDetailSpiciesRequest.swift
//  PokeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import Foundation


class GetPokemonDetailEvolutionRequest: RequestHandlerProtocol {
    
    private let chosenPokemonId: String
    
    init(chosenPokemonId: String) {
        self.chosenPokemonId = chosenPokemonId
    }
    
    func urlString(baseUrl: String) -> String {
        return baseUrl + "/evolution-chain/\(self.chosenPokemonId)"
    }
    
    func params() -> [String : String]? {
        return nil
    }
    
    func mapResult(data: Data) -> RequestResult<Any> {
        let decoder = JSONDecoder()
        do {
            let decoded = try decoder.decode(PokemonEvolutionModel.self, from: data)
            return RequestResult.success(decoded)
        } catch {
            return RequestResult.error(.jsonFormattedIncorrectly)
        }
    }
    
}
