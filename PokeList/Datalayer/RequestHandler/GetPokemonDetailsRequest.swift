//
//  GetPokeDetails.swift
//  PokeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import Foundation


class GetPokemonDetailsRequest: RequestHandlerProtocol {
    
    private let chosenPokemonId: String
    
    init(chosenPokemonId: String) {
        self.chosenPokemonId = chosenPokemonId
    }
    
    func urlString(baseUrl: String) -> String {
        return baseUrl + "/pokemon/\(self.chosenPokemonId)"
    }
    
    func params() -> [String : String]? {
        return nil
    }
    
    func mapResult(data: Data) -> RequestResult<Any> {

        let decoder = JSONDecoder()

        do {
            let decoded = try decoder.decode(PokemonDetailsModel.self, from: data)
            return RequestResult.success(decoded)
        } catch {
            return RequestResult.error(.jsonFormattedIncorrectly)
        }
    }
    
}
