//
//  GetPokeListRequest.swift
//  PókeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import Foundation

class GetPokemonListRequest: RequestHandlerProtocol {
    
    private let offset: Int
    
    init(offset: Int) {
        self.offset = offset
    }
    
    func urlString(baseUrl: String) -> String {
        return baseUrl + "/pokemon"
    }
    
    func params() -> [String : String]? {
        return [
            "limit": "20",
            "offset": "\(self.offset)"
        ]
    }
    
    func mapResult(data: Data) -> RequestResult<Any> {
        
        let decoder = JSONDecoder()
        do {
            let decoded = try decoder.decode(PokemonListModel.self, from: data)
            return RequestResult.success(decoded)
        } catch {
            print("Failed to decode JSON")
            return RequestResult.error(.jsonFormattedIncorrectly)
        }
    }
    
}
