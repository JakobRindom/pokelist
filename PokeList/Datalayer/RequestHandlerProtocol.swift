//
//  RequestHandlerProtocol.swift
//  PókeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import Foundation

protocol RequestHandlerProtocol {
    func urlString(baseUrl: String) -> String
    func params() -> [String: String]?
    func mapResult(data: Data) -> RequestResult<Any>
}
