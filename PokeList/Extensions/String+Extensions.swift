//
//  String+Extensions.swift
//  PokeList
//
//  Created by Jakob Rindom on 14/01/2021.
//

import Foundation

extension String {
    
    /// Returns the last component from a url
    func getLastCompunentFromUrlString() -> String? {
        if let url = URL(string: self)  {
            return url.lastPathComponent
        }
        return nil
    }
}
