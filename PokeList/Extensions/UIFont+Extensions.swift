//
//  UIFont+Extensions.swift
//  PokeList
//
//  Created by Jakob Rindom on 12/01/2021.
//

import UIKit

/*
 UIFontExtension. Used to set global UIFont's for the project.
 */


extension UIFont {
    
    static let pokemonName              = UIFont.systemFont(ofSize: 25, weight: .bold)
    static let specificationKey         = UIFont.systemFont(ofSize: 15, weight: .light)
    static let specificationValue       = UIFont.systemFont(ofSize: 15, weight: .regular)
    static let detailHeader             = UIFont.systemFont(ofSize: 18, weight: .bold)
 
}
