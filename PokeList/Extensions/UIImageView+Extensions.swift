//
//  UIImageView+Extensions.swift
//  PokeList
//
//  Created by Jakob Rindom on 12/01/2021.
//

import Foundation
import UIKit

class ImageStore: NSObject {
    static let imageCache = NSCache<NSString, UIImage>()
}

extension UIImageView {
    func image(from imageUrl: String?, completion: ((UIImage?) -> (Void))? = nil)  {
        DispatchQueue.global().async { [weak self] in
            guard let stringURL = imageUrl, let url = URL(string: stringURL) else {
                print("Invalid image url: \(String(describing: imageUrl))")
                return
            }
            func setImage(image:UIImage?) {
                DispatchQueue.main.async {
                    if let completion = completion {
                        completion(image)
                    } else {
                        self?.image = image
                    }
                }
            }
            let urlToString = url.absoluteString as NSString
            if let cachedImage = ImageStore.imageCache.object(forKey: urlToString) {
                setImage(image: cachedImage)
            } else if let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    ImageStore.imageCache.setObject(image, forKey: urlToString)
                    setImage(image: image)
                    
                }
            }else {
                print("Error getting image from url: \(String(describing: imageUrl))")
                setImage(image: nil)
            }
        }
    }
}
