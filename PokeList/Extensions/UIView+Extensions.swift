//
//  UIView+Extensions.swift
//  PokeList
//
//  Created by Jakob Rindom on 13/01/2021.
//

import UIKit

extension UIView {
    
    func applySketchShadow(color: UIColor = .black, alpha: Float = 0.5, x: CGFloat = 0, y: CGFloat = 2, blur: CGFloat = 4, spread: CGFloat = 0, cornerRadius: CGFloat = 0) {
        self.clipsToBounds = true
        self.layer.shadowColor = color.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowOpacity = alpha
        self.layer.shadowOffset = CGSize(width: x, height: y)
        self.layer.shadowRadius = blur / 2.0
        self.layer.cornerRadius = cornerRadius
        if spread == 0 {
            self.layer.shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            self.layer.shadowPath = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius ).cgPath
        }
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    
    
    /// Adds rounded corners to a view. Can add rounded corners the eg the top only.
    /// - Parameters:
    ///   - corners: UIRectCorner  - takes an array of specified corners: .topLeft, .topRight, .bottomLeft, .bottomRight or .allCorners
    ///   - radius: the radus value
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = getMaskedCorners(from: corners)
        } else {
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            let rect = self.bounds
            mask.frame = rect
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
    
    
    //Helper to get CACornerMask from UIRectCorner
    private func getMaskedCorners(from corners: UIRectCorner) -> CACornerMask {
        var maskedCorners: CACornerMask = []
        if corners.contains(.bottomLeft) {
            maskedCorners = maskedCorners.union(.layerMinXMaxYCorner)
        }
        if corners.contains(.bottomRight) {
            maskedCorners = maskedCorners.union(.layerMaxXMaxYCorner)
        }
        if corners.contains(.topLeft) {
            maskedCorners = maskedCorners.union(.layerMinXMinYCorner)
        }
        if corners.contains(.topRight) {
            maskedCorners = maskedCorners.union(.layerMaxXMinYCorner)
        }
        if corners.contains(.allCorners) {
            maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
        }
        return maskedCorners
    }
}
