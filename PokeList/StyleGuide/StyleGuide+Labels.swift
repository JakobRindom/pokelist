//
//  StyleGuide+Labels.swift
//  PokeList
//
//  Created by Jakob Rindom on 12/01/2021.
//

import Foundation

extension TextStyle {
    
    static let pokemonName              = TextStyle(font: .pokemonName, color: .black)
    static let specificationKey         = TextStyle(font: .specificationKey, color: .lightGray)
    static let specificationValue       = TextStyle(font: .specificationValue, color: .darkGray)
    static let detailHeader             = TextStyle(font: .detailHeader, color: .black)
    static let typeTitle                = TextStyle(font: .specificationValue, color: .black)
    
}

