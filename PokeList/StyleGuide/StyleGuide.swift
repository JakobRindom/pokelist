//
//  StyleGuide.swift
//  PokeList
//
//  Created by Jakob Rindom on 12/01/2021.
//

import UIKit

public struct TextStyle {
    let font: UIFont
    let color: UIColor
    
}

public struct ButtonStyle {
    let viewStyle: ViewStyle
    let textStyle: TextStyle
}

public struct ViewStyle {
    
    public struct LayerStyle {
        
        public struct BorderStyle {
            let color: UIColor
            let width: CGFloat
        }
        
        public struct ShadowStyle {
            let color: UIColor
            let radius: CGFloat
            let offset: CGSize
            let opacity: Float
        }
        
        let masksToBounds: Bool?
        let cornerRadius: CGFloat?
        let borderStyle: BorderStyle?
        let shadowStyle: ShadowStyle?
    }
    
    let backgroundColor: UIColor?
    let tintColor: UIColor?
    let layerStyle: LayerStyle?
}

struct ProgressStyle {
    let tintColor: UIColor
    let trackTintColor: UIColor?
}
