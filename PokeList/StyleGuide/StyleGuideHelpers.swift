//
//  StyleGuideHelper.swift
//  PokeList
//
//  Created by Jakob Rindom on 12/01/2021.
//

import UIKit

// MARK: Helper protocols
public protocol TextStyling {
    func textStyle(_ style: TextStyle)
}

public protocol ViewStyling {
    func viewStyle(_ style: ViewStyle)
    func viewStyle(for type: PokemonSpiciestype) -> ViewStyle
}

protocol ProgressStyling {
    func viewStyle(_ style: ProgressStyle)
}

public protocol ButtonStyling {
    func buttonStyle(_ style: ButtonStyle)
    func buttonStyle(for type: PokemonSpiciestype)
}

// MARK: Helper extensions
extension UIView: ViewStyling {
    
    
    public func viewStyle(_ style: ViewStyle) {
        if let backgroundColor = style.backgroundColor {
            self.backgroundColor = backgroundColor
        }
        if let tintColor = style.tintColor {
            self.tintColor = tintColor
        }
        if let layerStyle = style.layerStyle {
            if let cornerRadius = layerStyle.cornerRadius {
                self.layer.cornerRadius = cornerRadius
            }
            if let masksToBounds = layerStyle.masksToBounds {
                self.layer.masksToBounds = masksToBounds
            }
            if let borderStyle = layerStyle.borderStyle {
                self.layer.borderColor = borderStyle.color.cgColor
                self.layer.borderWidth = borderStyle.width
            }
            if let shadowStyle = layerStyle.shadowStyle {
                self.layer.shadowColor = shadowStyle.color.cgColor
                self.layer.shadowOffset = shadowStyle.offset
                self.layer.shadowRadius = shadowStyle.radius
                self.layer.shadowOpacity = shadowStyle.opacity
            }
        }
    }
    
    public func viewStyle(for type: PokemonSpiciestype) -> ViewStyle {
        return ViewStyle(backgroundColor: type.color(), tintColor: nil, layerStyle: nil)
    }
}

extension UILabel: TextStyling {
    public func textStyle(_ style: TextStyle) {
        font = style.font
        textColor = style.color
    }
}

extension UIButton: TextStyling, ButtonStyling {
    
    public func buttonStyle(_ style: ButtonStyle) {
        self.textStyle(style.textStyle)
        self.viewStyle(style.viewStyle)
    }
    
    public func textStyle(_ style: TextStyle) {
        titleLabel?.font = style.font
        setTitleColor(style.color, for: .normal)
    }
    
    public func buttonStyle(for type: PokemonSpiciestype)  {
        let buttonStyle = ButtonStyle(viewStyle: self.viewStyle(for: type) , textStyle: .typeTitle)
        self.buttonStyle(buttonStyle)
    }
}

extension UITextField: TextStyling {
    public func textStyle(_ style: TextStyle) {
        font = style.font
        textColor = style.color
    }
}

extension UITextView: TextStyling {
    public func textStyle(_ style: TextStyle) {
        font = style.font
        textColor = style.color
    }
}

extension UIProgressView: ProgressStyling {
    func viewStyle(_ style: ProgressStyle) {
        tintColor = style.tintColor
        trackTintColor = style.trackTintColor
    }
}
