//
//  DetailCoverCell.swift
//  PokeList
//
//  Created by Jakob Rindom on 12/01/2021.
//

import UIKit

class DetailCoverCell: UITableViewCell {

    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblName.textStyle(.pokemonName)
        self.backgroundColor = .clear
    }
    
    func setup(with model: PokeDetails.DetailCoverCell )  {
        self.imgCover.image(from: model.imageUrl)
        self.lblName.text = model.name.capitalized
    }
    
}
