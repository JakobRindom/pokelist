//
//  DetailEvolutionCell.swift
//  PokeList
//
//  Created by Jakob Rindom on 14/01/2021.
//

import UIKit

class DetailEvolutionCell: UITableViewCell {
    
    @IBOutlet weak var lblLevel: UILabel!
    @IBOutlet weak var lblPokemonId: UILabel!
    @IBOutlet weak var lblPokemomName: UILabel!
    @IBOutlet weak var viewLevel: UIView!
    @IBOutlet weak var imgPokemon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblLevel.textStyle(.specificationValue)
        self.lblPokemonId.textStyle(.specificationKey)
        self.lblPokemomName.textStyle(.specificationValue)
        
        self.backgroundColor = .clear
    }
    
    func setup(with model: PokeDetails.DetailEvolutionCell )  {
        if let level = model.level {
            self.lblLevel.text = level
        } else {
            self.viewLevel.isHidden = true
        }
        self.lblPokemomName.text = model.name
        self.lblPokemonId.text = model.id
        self.imgPokemon.image(from: model.imageUrl)
    }
    
}
