//
//  DetailSpecificationCell.swift
//  PokeList
//
//  Created by Jakob Rindom on 12/01/2021.
//

import UIKit

class DetailSpecificationCell: UITableViewCell {
    
    @IBOutlet weak var lblSpecificationTitle: UILabel!
    @IBOutlet weak var lblSpecificationValue: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblSpecificationTitle.textStyle(.specificationKey)
        self.lblSpecificationValue.textStyle(.specificationValue)
        self.backgroundColor = .clear
    }
    
    func setup(with model: PokeDetails.DetailSpecificationCell )  {
        self.lblSpecificationTitle.text = model.title
        self.lblSpecificationValue.text = model.value
    }
    
    
}
