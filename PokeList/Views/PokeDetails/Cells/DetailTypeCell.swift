//
//  DetailTypeCell.swift
//  PokeList
//
//  Created by Jakob Rindom on 14/01/2021.
//

import UIKit

class DetailTypeCell: UITableViewCell {
    
    @IBOutlet weak var btnOne: UIButton!
    @IBOutlet weak var btnTwo: UIButton!
    @IBOutlet weak var lblSpecificationTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblSpecificationTitle.textStyle(.specificationKey)
        self.btnOne.isEnabled = false
        self.btnTwo.isEnabled = false
        self.backgroundColor = .clear
        
        // Initialization code
    }
    
    func setup(with model: PokeDetails.DetailTypeCell )  {
        self.lblSpecificationTitle.text = model.title
        if let typeOne = model.typeOne {
            self.btnOne.buttonStyle(for: typeOne)
            self.btnOne.setTitle(typeOne.rawValue.capitalized, for: .normal)
            self.btnOne.isHidden = false
        } else {
            self.btnOne.isHidden = true
        }
        
        if let typeTwo = model.typeTwo {
            self.btnTwo.buttonStyle(for: typeTwo)
            self.btnTwo.setTitle(typeTwo.rawValue.capitalized, for: .normal)
            self.btnTwo.isHidden = false
        } else {
            self.btnTwo.isHidden = true
        }
    }
    
}
