//
//  DetailsSpecificationHeaderCell.swift
//  PokeList
//
//  Created by Jakob Rindom on 12/01/2021.
//

import UIKit

class DetailsSpecificationHeaderCell: UITableViewCell {
    
    @IBOutlet weak var lblSpecificationTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblSpecificationTitle.textStyle(.detailHeader)
        self.backgroundColor = .clear
    }
    
    func setup(with model: PokeDetails.DetailSpecificationHeaderCell)  {
        self.lblSpecificationTitle.text = model.title
    }
    
}
