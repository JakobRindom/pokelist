//
//  PokeDetailsInteractor.swift
//  PokeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import UIKit

protocol PokeDetailsBusinessLogic {
    func getPokemonDetails(request: PokeDetails.Details.Request)
}

protocol PokeDetailsDataStore {
    var chosenPokemonId: String { get set }
}

class PokeDetailsInteractor: PokeDetailsBusinessLogic, PokeDetailsDataStore {
    
    var presenter: PokeDetailsPresentationLogic?
    var chosenPokemonId: String = ""
    var detailEvolutionRequest: ((_ result: RequestResult<PokemonEvolutionModel>) -> Void)?
    
    
    // MARK: Get data from API
    
    func getPokemonDetails(request: PokeDetails.Details.Request) {        
        var details: PokemonDetailsModel?
        var detailSpicies: PokemonDetailSpiciesModel?
        var detailEvolution: PokemonEvolutionModel?
        
        /// Api call waits for all to finish
        let group = DispatchGroup()
        group.enter()
        group.enter()
        group.enter()
        
        detailEvolutionRequest = { result in
            switch result {
            case .success(let model):
                detailEvolution = model
            case .error(let error):
                print(error.localizedDescription)
            case .offline:
                break
            }
            group.leave()
        }
        
        
        Datalayer.shared.getPokemonDetails(chosenPokemonId: self.chosenPokemonId) { result in
            switch result {
            case .success(let model):
                details = model
            case .error(let error):
                print(error.localizedDescription)
            case .offline:
                break
            }
            group.leave()
        }
        
        Datalayer.shared.getPokemonDetailSpecies(chosenPokemonId: self.chosenPokemonId) { result in
            switch result {
            case .success(let model):
                detailSpicies = model
                if let evolutionId = model.evolutionChain?.url?.getLastCompunentFromUrlString() {
                    self.getEvolutionChain(for: evolutionId)
                }
            case .error(let error):
                print(error.localizedDescription)
            case .offline:
                break
            }
            group.leave()
        }
        
        group.notify(queue: .main, execute: {
            if let details = details, let detailSpicies = detailSpicies, let detailEvolution = detailEvolution {
                let response = PokeDetails.Details.Response(details: details, detailSpicies: detailSpicies, detailEvolution: detailEvolution)
                self.presenter?.presentPokemonDetails(response: response)
            }
        })
    }
    
    func getEvolutionChain(for id: String) {
        Datalayer.shared.getPokemonEvolutionChain(chosenPokemonId: id) { result in
            if let detailEvolutionRequest = self.detailEvolutionRequest {
                detailEvolutionRequest(result)
            }
        }
    }
    
    
}
