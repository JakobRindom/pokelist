//
//  PokeDetailsModels.swift
//  PokeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import UIKit

enum DetailCellType {
    case cover(PokeDetails.DetailCoverCell)
    case type(PokeDetails.DetailTypeCell)
    case specification(PokeDetails.DetailSpecificationCell)
    case specificationHeader(PokeDetails.DetailSpecificationHeaderCell)
    case evolution(PokeDetails.DetailEvolutionCell)
}

enum PokeDetails {
    // MARK: Use cases
    
    enum Details {
        struct Request {
        }
        struct Response {
            var details: PokemonDetailsModel
            var detailSpicies: PokemonDetailSpiciesModel
            var detailEvolution: PokemonEvolutionModel
        }
        struct ViewModel {
            var detailCells: [DetailCellType]
        }
    }
    
    
    // MARK: celltypes (All DetailCellProtocol)
    
    struct DetailCoverCell {
        var name: String
        var imageUrl: String
    }
    
    struct DetailTypeCell {
        var title: String
        var typeOne: PokemonSpiciestype?
        var typeTwo: PokemonSpiciestype?
    }
    
    struct DetailSpecificationCell {
        var title: String
        var value: String
    }
    
    struct DetailSpecificationHeaderCell {
        var title: String
    }
    
    struct DetailEvolutionCell {
        var name: String
        var level: String?
        var imageUrl: String
        var id: String
    }
    
}


