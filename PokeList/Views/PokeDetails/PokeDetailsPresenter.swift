//
//  PokeDetailsPresenter.swift
//  PokeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import UIKit

protocol PokeDetailsPresentationLogic {
    func presentPokemonDetails(response: PokeDetails.Details.Response)
    
}

class PokeDetailsPresenter: PokeDetailsPresentationLogic {
    weak var viewController: PokeDetailsDisplayLogic?
    
    // MARK: Present pokemon details
    
    func presentPokemonDetails(response: PokeDetails.Details.Response) {
        let cells = self.createCells(for: response)
        let viewModel = PokeDetails.Details.ViewModel(detailCells: cells)
        viewController?.displayPokemonDetails(viewModel: viewModel)
    }
    
    
    //MARK:  Create detail cells
    
    func createCells(for model: PokeDetails.Details.Response) -> [DetailCellType] {
        var arrCells: [DetailCellType] = []
        
        let cover = DetailCellType.cover(.init(name: model.details.name?.capitalized ?? "Unknown", imageUrl: model.details.sprites?.other?.officialArtwork?.frontDefault ?? "" ))
        arrCells.append(cover)
        
        let data = DetailCellType.specificationHeader(.init(title: "Pékemon Data:"))
        arrCells.append(data)
        
        let id = DetailCellType.specification(.init(title: "National n⍛", value: String(format: "%03d", model.details.id ?? 0)))
        arrCells.append(id)
        
        let types = self.getTypes(for: model)
        
        let type = DetailCellType.type(.init(title: "Type", typeOne: types.type1 , typeTwo: types.type2))
        arrCells.append(type)
        
        let spiciesEng = model.detailSpicies.genera?.first(where: { $0.language?.name == "en"  })
        let spicies = DetailCellType.specification(.init(title: "Spicies", value: spiciesEng?.genus ?? "Unknown" ))
        arrCells.append(spicies)
        
        let height = DetailCellType.specification(.init(title: "Height", value: "\(Float(model.details.height ?? 0)*0.1) m"))
        arrCells.append(height)
        
        let weight = DetailCellType.specification(.init(title: "Weight", value: "\(Float(model.details.weight ?? 0)*0.1) kg"))
        arrCells.append(weight)
        
        let abilities = DetailCellType.specification(.init(title: "Abilities", value: self.getAbilities(for: model)))
        arrCells.append(abilities)
        
        /// Add training specifications
        let training = self.createTrainingCells(for: model)
        arrCells.append(contentsOf: training)
        
        /// Add training baseStat
        let stat = self.getStats(for: model)
        arrCells.append(contentsOf: stat)
        
        let evolution = self.getEvolutionChain(for: model)
        arrCells.append(contentsOf: evolution)
        

        
        return arrCells
    }
    
   /*
    private func createCells(for model: PokeDetails.Details.Response) -> [DetailCellProtocol] {
        var arrCells: [DetailCellProtocol] = []
        
        let cover = PokeDetails.DetailCoverCell(name: model.details.name?.capitalized ?? "Unknown", imageUrl: model.details.sprites?.other?.officialArtwork?.frontDefault ?? "" )
        arrCells.append(cover)
        
        let data = PokeDetails.DetailSpecificationHeaderCell(title: "Pékemon Data:")
        arrCells.append(data)
        
        let id = PokeDetails.DetailSpecificationCell(title: "National n⍛", value: String(format: "%03d", model.details.id ?? 0))
        arrCells.append(id)
        
        let types = self.getTypes(for: model)
        let type = PokeDetails.DetailTypeCell(title: "Type", typeOne: types.type1 , typeTwo: types.type2)
        arrCells.append(type)
        
        let spiciesEng = model.detailSpicies.genera?.first(where: { $0.language?.name == "en"  })
        let spicies = PokeDetails.DetailSpecificationCell(title: "Spicies", value: spiciesEng?.genus ?? "Unknown" )
        arrCells.append(spicies)
        
        let height = PokeDetails.DetailSpecificationCell(title: "Height", value: "\(Float(model.details.height ?? 0)*0.1) m")
        arrCells.append(height)
        
        let weight = PokeDetails.DetailSpecificationCell(title: "Weight", value: "\(Float(model.details.weight ?? 0)*0.1) kg")
        arrCells.append(weight)
        
        let abilities = PokeDetails.DetailSpecificationCell(title: "Abilities", value: self.getAbilities(for: model))
        arrCells.append(abilities)
        
        /// Add training specifications
        let training = self.createTrainingCells(for: model)
        arrCells.append(contentsOf: training)
        
        /// Add training baseStat
        let stat = self.getStats(for: model)
        arrCells.append(contentsOf: stat)
        
        let evolution = self.getEvolutionChain(for: model)
        arrCells.append(contentsOf: evolution)
        
        return arrCells
    }
    */
    
    func createTrainingCells(for model: PokeDetails.Details.Response) -> [DetailCellType]  {
        var arrCells: [DetailCellType] = []
        
        let training = DetailCellType.specificationHeader(.init(title: "Training:"))
        arrCells.append(training)
        
        let capRate = DetailCellType.specification(.init(title: "Catch rates", value: "\(model.detailSpicies.captureRate ?? 0)"))
        arrCells.append(capRate)
        
        let growthRate = DetailCellType.specification(.init(title: "Growth Rate", value: "\(model.detailSpicies.growthRate?.name ?? "Unknown")"))
        arrCells.append(growthRate)
        
        let baseExp = DetailCellType.specification(.init(title: "Base Exp", value: "\(model.details.baseExperience ?? 0)"))
        arrCells.append(baseExp)
        
        return arrCells
    }
    
    private func getAbilities(for model: PokeDetails.Details.Response) -> String {
        var abilitiesString: String = ""
        guard let abilities = model.details.abilities else { return "None" }
        for ability in abilities {
            var name = ability.ability?.name?.capitalized ?? "None"
            name += ability.isHidden ?? false ? " (hidden ability)" : ""
            if abilities.last?.ability?.name != ability.ability?.name {
                name += "\n"
            }
            abilitiesString += name
        }
        return abilitiesString
    }
    
    func getStats(for model: PokeDetails.Details.Response) -> [DetailCellType] {
        
        var arrCells: [DetailCellType] = []
        
        let statTitle = DetailCellType.specificationHeader(.init(title: "Base stats:"))
        arrCells.append(statTitle)
        
        guard let stats = model.details.stats else { return [] }
        var total = 0
        for stat in stats {
            total += stat.baseStat ?? 0
            let statCell = DetailCellType.specification(.init(title: stat.stat?.name?.capitalized ?? "" , value: "\(stat.baseStat ?? 0)"))
            arrCells.append(statCell)
        }
        
        let totalCell = DetailCellType.specification(.init(title: "total", value: "\(total)"))
        arrCells.append(totalCell)
        
        
        return arrCells
    }
    
    private func getTypes(for model: PokeDetails.Details.Response) -> (type1: PokemonSpiciestype?, type2: PokemonSpiciestype?) {
        if let types = model.details.types {
            switch types.count {
            case 1:
                return (type1: types[0].type?.name, type2: nil)
            case 2:
                return (type1: types[0].type?.name, type2: types[1].type?.name)
            default:
                return (type1: nil, type2: nil)
            }
        }
        return (type1: nil, type2: nil)
    }
    
    
    private func getEvolutionChain(for model: PokeDetails.Details.Response) -> [DetailCellType] {
        var arrCells: [DetailCellType] = []
        guard let generations = model.detailEvolution.chain else { return [] }
        
        let evolutionChartTitle = DetailCellType.specificationHeader(.init(title: "Evolution chart:"))
        arrCells.append(evolutionChartTitle)
        
        var generation: Chain? = generations
        
        /// evolution are nested inside eachother
        repeat {
            guard let generationChain = generation else {  return arrCells }
            
            let name = generationChain.species?.name?.capitalized ?? "Unknown"
            
            if  let urlStr = generationChain.species?.url, let id = urlStr.getLastCompunentFromUrlString()  {
                let imageUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/\(id).png"
                
                let level = generationChain.evolutionDetails?.first?.minLevel
                let idInt = Int(id)
                let cell = DetailCellType.evolution(.init(name: name, level: (level != nil) ? "(Level \(level ?? 0))" : nil , imageUrl: imageUrl, id: "#\(String(format: "%03d", idInt ?? 0 ))"))
                arrCells.append(cell)
            }
            generation = generationChain.evolvesTo?.first
            
        } while generation != nil
        
        return arrCells
    }
    
    
    
}
