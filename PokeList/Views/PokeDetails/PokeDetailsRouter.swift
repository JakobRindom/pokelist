//
//  PokeDetailsRouter.swift
//  PokeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import UIKit

protocol PokeDetailsRoutingLogic {
}

protocol PokeDetailsDataPassing {
    var dataStore: PokeDetailsDataStore? { get }
}

class PokeDetailsRouter: NSObject, PokeDetailsRoutingLogic, PokeDetailsDataPassing {
    weak var viewController: PokeDetailsViewController?
    var dataStore: PokeDetailsDataStore?

}
