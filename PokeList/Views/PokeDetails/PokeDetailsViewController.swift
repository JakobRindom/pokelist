//
//  PokeDetailsViewController.swift
//  PokeList
//
//  Created by Jakob Rindom on 11/01/2021.
//


import UIKit

protocol PokeDetailsDisplayLogic: class {
    func displayPokemonDetails(viewModel: PokeDetails.Details.ViewModel)
}

class PokeDetailsViewController: UIViewController, PokeDetailsDisplayLogic {
    var interactor: PokeDetailsBusinessLogic?
    var router: (NSObjectProtocol & PokeDetailsRoutingLogic & PokeDetailsDataPassing)?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = PokeDetailsInteractor()
        let presenter = PokeDetailsPresenter()
        let router = PokeDetailsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    
    // MARK: IBOutlets
    
    @IBOutlet weak var tableviewDetails: UITableView!

    // MARK: Variables
    
    var arrDetailCells: [DetailCellType] = []
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableview()
        getPokemonDetails()
    }
    
    func setupTableview()  {
        self.tableviewDetails.delegate = self
        self.tableviewDetails.dataSource = self
        
        self.tableviewDetails.register(UINib(nibName: "DetailCoverCell", bundle: nil), forCellReuseIdentifier: "DetailCoverCell")
        self.tableviewDetails.register(UINib(nibName: "DetailSpecificationCell", bundle: nil), forCellReuseIdentifier: "DetailSpecificationCell")
        self.tableviewDetails.register(UINib(nibName: "DetailsSpecificationHeaderCell", bundle: nil), forCellReuseIdentifier: "DetailsSpecificationHeaderCell")
        self.tableviewDetails.register(UINib(nibName: "DetailTypeCell", bundle: nil), forCellReuseIdentifier: "DetailTypeCell")
        self.tableviewDetails.register(UINib(nibName: "DetailEvolutionCell", bundle: nil), forCellReuseIdentifier: "DetailEvolutionCell")
        
        self.tableviewDetails.backgroundColor = .white
        tableviewDetails.separatorStyle = .none
        self.tableviewDetails.allowsSelection = false
    }
    
    // MARK: Get data from Interactor
        
    func getPokemonDetails() {
        let request = PokeDetails.Details.Request()
        interactor?.getPokemonDetails(request: request)
    }
    
    // MARK: Display data
    
    func displayPokemonDetails(viewModel: PokeDetails.Details.ViewModel) {
        self.arrDetailCells = viewModel.detailCells
        self.tableviewDetails.reloadData()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension PokeDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDetailCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = self.arrDetailCells[indexPath.row]
        
        switch data {
        case .cover(let model):
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCoverCell") as! DetailCoverCell
            cell.setup(with: model)
            return cell
        case .type(let model):
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTypeCell") as! DetailTypeCell
            cell.setup(with: model)
            return cell
        case .specification(let model):
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailSpecificationCell") as! DetailSpecificationCell
            cell.setup(with: model)
            return cell
        case .specificationHeader(let model):
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsSpecificationHeaderCell") as! DetailsSpecificationHeaderCell
            cell.setup(with: model)
            return cell
        case .evolution(let model):
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailEvolutionCell") as! DetailEvolutionCell
            cell.setup(with: model)
            return cell
        }
    }
    
}
