//
//  PokemonListCell.swift
//  PokeList
//
//  Created by Jakob Rindom on 12/01/2021.
//

import UIKit

class PokemonListCell: UICollectionViewCell {
    
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgCover: UIImageView!
    
    // Offset to Create parallax Effect
    var imageOffset: CGPoint = CGPoint(x: 0, y: 0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblName.textStyle(.pokemonName)
        self.applySketchShadow(color: .black, alpha: 0.6, x: 3 , y: 4, blur: 4, spread: 0, cornerRadius: 14)
        self.viewBackground.roundCorners(corners: [.bottomRight, .topLeft ] , radius: 14)
    }
    
    func setupCell(data: PokemonListModel.Pokemons )  {
        self.lblName.text = data.name?.capitalized
        /// HACK to get image for pokemon
        if let urlStr = data.url, let id = urlStr.getLastCompunentFromUrlString()  {
            let imageUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/\(id).png"
            UIImageView().image(from: imageUrl) { image in
                self.imgCover.image = image
                self.setImageOffset(imageOffset: self.imageOffset)
            }
        }
    }
    
    func setImageOffset(imageOffset: CGPoint) {
        self.imageOffset = imageOffset
        let frame: CGRect = imgCover.bounds
        let offsetFrame: CGRect = frame.offsetBy(dx: imageOffset.x, dy: imageOffset.y)
        imgCover.frame = offsetFrame
    }
    
    override func prepareForReuse() {
        self.imgCover.image = nil
        self.lblName.text = ""
        super.prepareForReuse()
    }
}
