//
//  PokeListInteractor.swift
//  PókeList
//
//  Created by Jakob Rindom on 11/01/2021.
//

import UIKit

protocol PokeListBusinessLogic {
    func getPokemonList(request: PokeList.GetList.Request)
    func showDetails(request: PokeList.ShowDetails.Request)
}

protocol PokeListDataStore {
    var chosenPokemonId: String { get set }
}

class PokeListInteractor: PokeListBusinessLogic, PokeListDataStore {
    
    var presenter: PokeListPresentationLogic?
    var chosenPokemonId: String = ""
    
    // MARK: Call Api
    
    func getPokemonList(request: PokeList.GetList.Request) {
        Datalayer.shared.getPokoList(offset: request.offset) { result in
            switch result {
            case .success(let model):
                let response = PokeList.GetList.Response(pokemons: model.results , newOffset: request.offset + model.results.count , totalCount: model.count)
                self.presenter?.presentPokemonList(response: response)
            case .error(let error):
                print(error.localizedDescription)
            case .offline:
                break
            }
        }
    }
    
    // Set chosen chosenPokemonId for passing to Details
    func showDetails(request: PokeList.ShowDetails.Request) {
        self.chosenPokemonId = request.chosenPokemonId
        let response = PokeList.ShowDetails.Response()
        self.presenter?.presentDetails(response: response)
    }
    
}
