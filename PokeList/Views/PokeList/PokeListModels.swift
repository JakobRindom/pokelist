//
//  PokeListModels.swift
//  PókeList
//
//  Created by Jakob Rindom on 11/01/2021.
//


import UIKit

enum PokeList {
    // MARK: Use cases
    
    enum GetList {
        struct Request {
            var offset: Int
        }
        struct Response {
            var pokemons: [PokemonListModel.Pokemons]
            var newOffset: Int
            var totalCount: Int
        }
        struct ViewModel {
            var pokemons: [PokemonListModel.Pokemons]
            var newOffset: Int
            var totalCount: Int
        }
    }
    
    
    enum ShowDetails {
        struct Request {
            var chosenPokemonId: String
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    
    
}
