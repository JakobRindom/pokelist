//
//  PokeListPresenter.swift
//  PókeList
//
//  Created by Jakob Rindom on 11/01/2021.
//


import UIKit

protocol PokeListPresentationLogic {
    func presentPokemonList(response: PokeList.GetList.Response)
    func presentDetails(response: PokeList.ShowDetails.Response)
}

class PokeListPresenter: PokeListPresentationLogic {
    weak var viewController: PokeListDisplayLogic?
    
    // MARK: Prepare for display
    
    func presentPokemonList(response: PokeList.GetList.Response) {
        let viewModel = PokeList.GetList.ViewModel(pokemons: response.pokemons, newOffset: response.newOffset, totalCount: response.totalCount)
        viewController?.displayPokemonList(viewModel: viewModel)
    }
    
    func presentDetails(response: PokeList.ShowDetails.Response) {
        let viewModel = PokeList.ShowDetails.ViewModel()
        viewController?.displayDetails(viewModel: viewModel)
    }
}
