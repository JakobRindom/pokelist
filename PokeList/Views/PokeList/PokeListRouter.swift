//
//  PokeListRouter.swift
//  PókeList
//
//  Created by Jakob Rindom on 11/01/2021.
//


import UIKit

protocol PokeListRoutingLogic {
    func routeToDetatils()
}

protocol PokeListDataPassing {
    var dataStore: PokeListDataStore? { get }
}

class PokeListRouter: NSObject, PokeListRoutingLogic, PokeListDataPassing {
    weak var viewController: PokeListViewController?
    var dataStore: PokeListDataStore?
    
    // MARK: Routing
    
    func routeToDetatils() {
        let storyboard = UIStoryboard(name: "PokeDetails", bundle: nil)
            let destinationVC = storyboard.instantiateViewController(withIdentifier: "PokeDetailsViewController") as! PokeDetailsViewController
            var destinationDS = destinationVC.router!.dataStore!
        passDataToDetails(source: dataStore!, destination: &destinationDS)
        navigateToDetails(source: viewController!, destination: destinationVC)
    }
    
    // MARK: Navigation
    
    func navigateToDetails(source: PokeListViewController, destination: PokeDetailsViewController) {
      source.show(destination, sender: nil)
    }
    
    // MARK: Passing data
    
    func passDataToDetails(source: PokeListDataStore, destination: inout PokeDetailsDataStore) {
      destination.chosenPokemonId = source.chosenPokemonId
    }
}
