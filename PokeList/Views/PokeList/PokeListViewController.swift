//
//  PokeListViewController.swift
//  PókeList
//
//  Created by Jakob Rindom on 11/01/2021.
//


import UIKit

protocol PokeListDisplayLogic: class {
    func displayPokemonList(viewModel: PokeList.GetList.ViewModel)
    func displayDetails(viewModel: PokeList.ShowDetails.ViewModel)
}

class PokeListViewController: UIViewController, PokeListDisplayLogic {
    var interactor: PokeListBusinessLogic?
    var router: (NSObjectProtocol & PokeListRoutingLogic & PokeListDataPassing)?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = PokeListInteractor()
        let presenter = PokeListPresenter()
        let router = PokeListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    @IBOutlet weak var collectionViewPokeList: UICollectionView!
    
    var arrPokemonModel: [PokemonListModel.Pokemons] = []
    var currentOffset: Int = 0
    var loading: Bool = false
    var totalCount: Int = 0
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCollectionView()
        getPokemonList(with: currentOffset)
    }
    
    func setupCollectionView()  {
        self.collectionViewPokeList.dataSource = self
        self.collectionViewPokeList.delegate = self
        self.collectionViewPokeList.register(UINib(nibName: "PokemonListCell", bundle: nil), forCellWithReuseIdentifier: "PokemonListCell")
        self.collectionViewPokeList.backgroundColor = .clear
    }
    
    // MARK: Get data
    
    
    func getPokemonList(with offset: Int) {
        let request = PokeList.GetList.Request(offset: offset)
        interactor?.getPokemonList(request: request)
    }
    
    func displayPokemonList(viewModel: PokeList.GetList.ViewModel) {
        self.arrPokemonModel.append(contentsOf: viewModel.pokemons)
        self.currentOffset = viewModel.newOffset
        self.totalCount = viewModel.totalCount
        self.loading = false
        self.collectionViewPokeList.reloadData()
    }
    
    func displayDetails(viewModel: PokeList.ShowDetails.ViewModel) {
        self.router?.routeToDetatils()
    }
    
    func calculateImageOffset(with yOffset: CGFloat ) -> CGPoint {
        let center: CGFloat = (UIScreen.main.bounds.width-40)/2
        let xOffset = center - 40
        let yOffset: CGFloat = ((collectionViewPokeList.contentOffset.y - yOffset) / 120) * 7
        return CGPoint(x: xOffset, y: yOffset)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for view in collectionViewPokeList.visibleCells {
            if let view: PokemonListCell = view as? PokemonListCell {
                view.setImageOffset(imageOffset: self.calculateImageOffset(with: view.frame.origin.y))
            }
        }
    }
    
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension PokeListViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPokemonModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PokemonListCell", for: indexPath) as! PokemonListCell
        cell.imageOffset = self.calculateImageOffset(with: cell.frame.origin.y)
        if indexPath.row == self.arrPokemonModel.count - 5 && !loading && totalCount != self.arrPokemonModel.count {
            self.getPokemonList(with: self.currentOffset)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? PokemonListCell {
            let data = self.arrPokemonModel[indexPath.row]
            cell.setupCell(data: data)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let chosenPokemon = self.arrPokemonModel[indexPath.row]
        if let chosenPokemonId = chosenPokemon.url?.getLastCompunentFromUrlString() {
            let request = PokeList.ShowDetails.Request(chosenPokemonId: chosenPokemonId)
            self.interactor?.showDetails(request: request)
        }
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension PokeListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        return CGSize(width: screenSize.width-40, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}

